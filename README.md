## sys_tssi_64_armv82_infinix-user 13 TP1A.220624.014 473252 release-keys
- Manufacturer: infinix
- Platform: mt6893
- Codename: Infinix-X6731
- Brand: Infinix
- Flavor: sys_tssi_64_armv82_infinix-user
- Release Version: 13
- Kernel Version: 4.19.191
- Id: TP1A.220624.014
- Incremental: 231107V2593
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Infinix/X6731-GL/Infinix-X6731:12/SP1A.210812.016/231106V2054:user/release-keys
- OTA version: 
- Branch: sys_tssi_64_armv82_infinix-user-13-TP1A.220624.014-473252-release-keys
- Repo: infinix_infinix-x6731_dump
